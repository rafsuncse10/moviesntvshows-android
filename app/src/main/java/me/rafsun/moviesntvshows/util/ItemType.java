package me.rafsun.moviesntvshows.util;

/**
 * Created by Rafsun on 1/16/15.
 */
public enum ItemType {


    BOX_OFFICE("Box Office"),
    OPENING("Opening"),
    IN_THEATER("In Theaters"),
    UPCOMING("Upcoming");


    private String name;
    private ItemType(String movie){
        this.name = movie;
    }

    public String movie(){
        return name;
    }
}
