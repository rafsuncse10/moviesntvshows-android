package me.rafsun.moviesntvshows.util;

import android.os.Build;

import java.util.List;

import me.rafsun.moviesntvshows.Singletons.SingletonBoxOffice;
import me.rafsun.moviesntvshows.Singletons.SingletonInTheaters;
import me.rafsun.moviesntvshows.Singletons.SingletonOpening;
import me.rafsun.moviesntvshows.Singletons.SingletonUpcoming;
import me.rafsun.moviesntvshows.json.box.Movie;


/**
 * Created by Rafsun on 1/21/15.
 */
public class Utils {

    public static String EXTRA_PARAM_PARALLAX = "PARAM_PARALLAX";
    public static boolean hasJellyBean() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
    }



    public static ItemType getItemType(String MOVIE_TYPE) {

        ItemType mItemType = null;
        if(MOVIE_TYPE != null){
            if(MOVIE_TYPE.equals(ItemType.BOX_OFFICE.movie())) {
                mItemType = ItemType.BOX_OFFICE;
            }
            else if(MOVIE_TYPE.equals(ItemType.OPENING.movie())) {
                mItemType = ItemType.OPENING;
            }
            else if(MOVIE_TYPE.equals(ItemType.IN_THEATER.movie())) {
                mItemType = ItemType.IN_THEATER;
            }
            else if(MOVIE_TYPE.equals(ItemType.UPCOMING.movie())){
                mItemType = ItemType.UPCOMING;

            }
        }

        return mItemType;
    }

    public static List<Movie> getMovieListIntance(String MOVIE_TYPE) {

        List<Movie> movies = null;
        if(MOVIE_TYPE != null){
            if(MOVIE_TYPE.equals(ItemType.BOX_OFFICE.movie())) {
                movies = SingletonBoxOffice.INSTANCE.getInstance();
            }
            else if(MOVIE_TYPE.equals(ItemType.OPENING.movie())) {
                movies = SingletonOpening.INSTANCE.getInstance();
            }
            else if(MOVIE_TYPE.equals(ItemType.IN_THEATER.movie())) {
                movies = SingletonInTheaters.INSTANCE.getInstance();
            }
            else if(MOVIE_TYPE.equals(ItemType.UPCOMING.movie())){
                movies = SingletonUpcoming.INSTANCE.getInstance();

            }
        }

        return movies;

    }
}
