/*
 * Copyright 2014 Soichiro Kashima
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package me.rafsun.moviesntvshows.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.res.TypedArray;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView;

import java.util.ArrayList;
import java.util.List;

import me.rafsun.moviesntvshows.R;
import me.rafsun.moviesntvshows.json.box.BoxOffice;
import me.rafsun.moviesntvshows.json.box.Movie;
import me.rafsun.moviesntvshows.json.opening.Opening;
import me.rafsun.moviesntvshows.json.theater.InTheaters;
import me.rafsun.moviesntvshows.json.upcoming.Upcoming;
import me.rafsun.moviesntvshows.util.ItemType;
import me.rafsun.moviesntvshows.util.URL;

public abstract class BaseFragment extends Fragment {

    public static ArrayList<String> getDummyData() {
        return BaseActivity.getDummyData();
    }

    private MyApplication myApplication = MyApplication.getInstance();

    private  ProgressDialog pDialog;


    protected int getActionBarSize() {
        Activity activity = getActivity();
        if (activity == null) {
            return 0;
        }
        TypedValue typedValue = new TypedValue();
        int[] textSizeAttr = new int[]{R.attr.actionBarSize};
        int indexOfAttrTextSize = 0;
        TypedArray a = activity.obtainStyledAttributes(typedValue.data, textSizeAttr);
        int actionBarSize = a.getDimensionPixelSize(indexOfAttrTextSize, -1);
        a.recycle();
        return actionBarSize;
    }

    protected int getScreenHeight() {
        Activity activity = getActivity();
        if (activity == null) {
            return 0;
        }
        return activity.findViewById(android.R.id.content).getHeight();
    }

    //    protected void setDummyData(ListView listView) {
//        listView.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, getDummyData()));
//    }
//
//    protected void setDummyDataWithHeader(ListView listView, View headerView) {
//        listView.addHeaderView(headerView);
//        setDummyData(listView);
//    }
//
//    protected void setDummyData(GridView gridView) {
//        gridView.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, getDummyData()));
//    }
//
//    protected void setDummyData(RecyclerView recyclerView) {
//        recyclerView.setAdapter(new SimpleRecyclerAdapter(getActivity(), getDummyData()));
//    }
//
    protected void setDummyDataWithHeader(RecyclerView recyclerView, View headerView) {
        recyclerView.setAdapter(new SimpleHeaderRecyclerAdapter(getActivity(), getDummyData(), headerView));
    }

//    protected void populateAdapter(ItemType movieType , final ObservableRecyclerView recyclerView){
//    protected void populateAdapter(ItemType movieType,List<Movie> movies ,SimpleRecyclerAdapter adapter, ObservableRecyclerView recyclerView){
    protected void populateAdapter(final ItemType movieType,final List<Movie> movies ,SimpleRecyclerAdapter adapter,final ObservableRecyclerView recyclerView,final SwipeRefreshLayout swiper){

//        List<Movie> movies = new ArrayList<Movie>();

//        SimpleRecyclerAdapter adapter = new SimpleRecyclerAdapter(getActivity(),movies);

        recyclerView.setAdapter(adapter);
//        // We make sure that the SwipeRefreshLayout is displaying it's refreshing indicator
//        if (!swiper.isRefreshing()) {
//            swiper.setRefreshing(true);
//        }

        swiper.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                startJSONFetcher(movieType, movies, recyclerView,swiper);
            }
        });

//        if(myApplication.isFirst(movieType)) {
        if(movies.isEmpty()) {
//            pDialog = new ProgressDialog(getActivity());
//            // Showing progress dialog before making http request
//            pDialog.setMessage("Loading...");
//            pDialog.show();




//            startJSONFetcher(movieType, movies, recyclerView);
            startJSONFetcher(movieType, movies, recyclerView,swiper);
        }

    }



//    protected void startJSONFetcher(ItemType movieType, final List<Movie> movies, final ObservableRecyclerView recyclerView) {
    protected void startJSONFetcher(ItemType movieType, final List<Movie> movies, final ObservableRecyclerView recyclerView, final SwipeRefreshLayout swiper) {

//        Toast.makeText(getActivity(),"I'm Called "+ movies.size(),Toast.LENGTH_SHORT).show();
        GSonRequest gSonRequest = null;

        //show swiper at startup - http://stackoverflow.com/a/26910973
        swiper.post(new Runnable() {
            @Override
            public void run() {
                swiper.setRefreshing(true);
            }
        });

        switch (movieType) {

            case OPENING:
                gSonRequest = new GSonRequest(URL.url_opening, Opening.class, null,
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                if (response instanceof Opening) {
//                                    hidePDialog();
                                    swiper.setRefreshing(false);
                                    movies.clear();
                                    movies.addAll(((Opening) response).getMovies());
                                    recyclerView.getAdapter().notifyDataSetChanged();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

//                                hidePDialog();
                                swiper.setRefreshing(false);
                                Toast.makeText(getActivity(), "Unable to fetch", Toast.LENGTH_SHORT).show();
                                movies.clear();
                            }
                        }
                );
                break;
            case UPCOMING:
                gSonRequest = new GSonRequest(URL.url_upcoming, Upcoming.class, null,
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                if (response instanceof Upcoming) {
//                                    hidePDialog();
                                    swiper.setRefreshing(false);
                                    movies.clear();
                                    movies.addAll(((Upcoming) response).getMovies());
                                    recyclerView.getAdapter().notifyDataSetChanged();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getActivity(), "Unable to fetch", Toast.LENGTH_SHORT).show();
//                                    hidePDialog();
                                swiper.setRefreshing(false);
                                movies.clear();
                            }
                        }
                );

                break;
            case IN_THEATER:
                gSonRequest = new GSonRequest(URL.url_in_theaters, InTheaters.class, null,
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                if (response instanceof InTheaters){
//                                    hidePDialog();
                                    swiper.setRefreshing(false);
                                    movies.clear();
                                    movies.addAll(((InTheaters) response).getMovies());
                                recyclerView.getAdapter().notifyDataSetChanged();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getActivity(), "Unable to fetch", Toast.LENGTH_SHORT).show();
//                                    hidePDialog();
                                swiper.setRefreshing(false);
                                movies.clear();
                            }
                        }
                );

                break;
            case BOX_OFFICE:
                gSonRequest = new GSonRequest(URL.url_box_office, BoxOffice.class, null,
                        new Response.Listener() {
                            @Override
                            public void onResponse(Object response) {
                                if (response instanceof BoxOffice) {
//                                    hidePDialog();
                                    swiper.setRefreshing(false);
                                    movies.clear();
                                    movies.addAll(((BoxOffice) response).getMovies());
                                    recyclerView.getAdapter().notifyDataSetChanged();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Toast.makeText(getActivity(), "Unable to fetch", Toast.LENGTH_SHORT).show();
//                                    hidePDialog();
                                swiper.setRefreshing(false);
                                movies.clear();
                            }
                        }
                );

                break;

            default:
                break;

        }


        // Adding request to request queue
        gSonRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 3,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        myApplication.addToRequestQueue(gSonRequest);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

}
