/*
 * Copyright 2014 Soichiro Kashima
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.rafsun.moviesntvshows.ui;

import android.annotation.TargetApi;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;
import java.util.Random;

import me.rafsun.moviesntvshows.R;
import me.rafsun.moviesntvshows.json.box.Movie;
import me.rafsun.moviesntvshows.util.ItemType;
import me.rafsun.moviesntvshows.util.Utils;


public class SimpleRecyclerAdapter extends RecyclerView.Adapter<SimpleRecyclerAdapter.ViewHolder> implements View.OnClickListener {
    private LayoutInflater mInflater;
    private List<Movie> mItems;
    private Context context;
    private int[] randomsaved;
    private int imgWidthDP ;
    private int imgHeightDP;
    private ItemType mItemType;

    private int[] resources = new int[]{
            R.drawable.placeholder_image1,
            R.drawable.placeholder_image2,
            R.drawable.placeholder_image3
    };

    public SimpleRecyclerAdapter(Context context,ItemType mItemType, List<Movie> items) {
        this.context = context;
        this.mItemType = mItemType;
        mInflater = LayoutInflater.from(context);
        mItems = items;
        randomsaved = new int[]{
          new Random().nextInt(resources.length -1),
          new Random().nextInt(resources.length -1),
          new Random().nextInt(resources.length -1)
        };

        imgWidthDP = (int)context.getResources().getDimension(R.dimen.column_width);
        imgHeightDP = (int)context.getResources().getDimension(R.dimen.column_height2);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Create a new view by inflating the row item xml.
        View v = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.movie_grid, parent, false);

        ViewHolder holder = new ViewHolder(v);

        // Sets the click adapter for the entire cell
        // to the one in this class.
        holder.itemView.setOnClickListener(SimpleRecyclerAdapter.this);
        holder.itemView.setTag(holder);

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

//        StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) viewHolder.itemView.getLayoutParams();
//        layoutParams.getSpanIndex();

        if (!mItems.isEmpty()) {
            viewHolder.mvTitle.setText(mItems.get(position).getTitle());

            Glide.with(context)
                    .load(mItems.get(position).getPosters().getDetailed().replaceAll("tmb.jpg","det.jpg") )
                    .centerCrop()
                    .crossFade()
//                    .preload(imgWidthDP,imgHeightDP)
                    .into(viewHolder.mvImageView);

//            if(position+1 % 3 == 0)
//            viewHolder.mvImageView.setImageResource( resources[randomsaved[2]] );
//            else if(position+1 % 5 == 0)
//                viewHolder.mvImageView.setImageResource( resources[0] );

        }

    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onClick(View v) {
        ViewHolder holder = (ViewHolder) v.getTag();
        int position = holder.getPosition();

//        Toast.makeText(context, "Item : " + position, Toast.LENGTH_SHORT).show();

        MyApplication.getInstance().setParallaxActivityCalled(true);

//        final Intent i = new Intent(context, ParallaxToolbarScrollViewActivity.class);
        final Intent i = new Intent(context, SlidingUpScrollViewActivity.class);


        String intent_params_parallax[]  = new String[]{
                mItemType.movie(),
                Integer.toString(position)
        };


        i.putExtra( Utils.EXTRA_PARAM_PARALLAX, intent_params_parallax );

//                i.putExtra(ImageDetailActivity.EXTRA_IMAGE, (int) id);
        if (Utils.hasJellyBean()) {
            // makeThumbnailScaleUpAnimation() looks kind of ugly here as the loading spinner may
            // show plus the thumbnail image in GridView is cropped. so using
            // makeScaleUpAnimation() instead.
            ActivityOptions options =
                    ActivityOptions.makeScaleUpAnimation(v, 0, 0, v.getWidth(), v.getHeight());
//                        ActivityOptions.makeSceneTransitionAnimation(, v, "SimpleRecyclerVIew");
            context.startActivity(i, options.toBundle());
        } else {
            context.startActivity(i);
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mvImageView;
        TextView mvTitle;

        public ViewHolder(View itemview) {
            super(itemview);
            mvImageView = (ImageView) itemview.findViewById(R.id.mvImageView);
            mvTitle = (TextView) itemview.findViewById(R.id.mvTitle);
        }
    }
}
