/*
 * Copyright 2014 Soichiro Kashima
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.rafsun.moviesntvshows.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.ViewTarget;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.github.ksoichiro.android.observablescrollview.Scrollable;
import com.github.ksoichiro.android.observablescrollview.TouchInterceptionFrameLayout;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.animation.ValueAnimator;
import com.nineoldandroids.view.ViewHelper;
import com.nineoldandroids.view.ViewPropertyAnimator;

import java.util.List;

import me.rafsun.moviesntvshows.R;
import me.rafsun.moviesntvshows.json.box.Abridged_cast;
import me.rafsun.moviesntvshows.json.box.Movie;
import me.rafsun.moviesntvshows.json.youtube.YoutubeQuery;
import me.rafsun.moviesntvshows.util.ItemType;
import me.rafsun.moviesntvshows.util.URL;
import me.rafsun.moviesntvshows.util.Utils;


public abstract class SlidingUpBaseActivity<S extends Scrollable> extends BaseActivity implements ObservableScrollViewCallbacks {

    private static final String STATE_SLIDING_STATE = "slidingState";
    public static final String TAG = SlidingUpBaseActivity.class.getSimpleName();

    private static final int SLIDING_STATE_TOP = 0;
    private static final int SLIDING_STATE_MIDDLE = 1;
    private static final int SLIDING_STATE_BOTTOM = 2;


    private Handler handler;
    private Thread thread;
    private View mHeader;
    private View mHeaderBar;
    private View mHeaderOverlay;
    private View mHeaderFlexibleSpace;
    private TextView mTitle;
    private TextView mToolbarTitle;
    private View mImageView;
    private View mFab;
    private Toolbar mToolbar;
    private S mScrollable;
    private TouchInterceptionFrameLayout mInterceptionLayout;

    // Fields that just keep constants like resource values
    private int mActionBarSize;
    private int mIntersectionHeight;
    private int mHeaderBarHeight;
    private int mSlidingSlop;
    private int mSlidingHeaderBlueSize;
    private int mColorPrimary;
    private int mFlexibleSpaceImageHeight;
    private int mToolbarColor;
    private int mFabMargin;

    // Fields that needs to saved
    private int mSlidingState;

    // Temporary states
    private boolean mFabIsShown;
    private boolean mMoved;
    private float mInitialY;
    private float mMovedDistanceY;
    private float mScrollYOnDownMotion;

    // These flags are used for changing header colors.
    private boolean mHeaderColorIsChanging;
    private boolean mHeaderColorChangedToBottom;
    private boolean mHeaderIsAtBottom;
    private boolean mHeaderIsNotAtBottom;

    private int mParallaxImageHeight;
    private Movie movie;
    private ItemType itemType;
    private String intent_params_parallax[];
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());
        context = this;
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        handler = new Handler();
        setSupportActionBar(mToolbar);
        ViewHelper.setScaleY(mToolbar, 0);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");

        mToolbarColor = getResources().getColor(R.color.primary);
        mToolbar.setBackgroundColor(Color.TRANSPARENT);
        mToolbar.setTitle("");

        mFlexibleSpaceImageHeight = getResources().getDimensionPixelSize(R.dimen.flexible_space_image_height);
        mIntersectionHeight = getResources().getDimensionPixelSize(R.dimen.intersection_height);
        mActionBarSize = getActionBarSize();
//        mHeaderBarHeight = getResources().getDimensionPixelSize(R.dimen.header_bar_height);
        mHeaderBarHeight = mActionBarSize;
        mSlidingSlop = getResources().getDimensionPixelSize(R.dimen.sliding_slop);
        mColorPrimary = getResources().getColor(R.color.primary);
        mSlidingHeaderBlueSize = getResources().getDimensionPixelSize(R.dimen.sliding_overlay_blur_size);

        mHeader = findViewById(R.id.header);
        mHeaderBar = findViewById(R.id.header_bar);
        mHeaderOverlay = findViewById(R.id.header_overlay);
        mHeaderFlexibleSpace = findViewById(R.id.header_flexible_space);
        mImageView = findViewById(R.id.image);
        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slideOnClick();
            }
        });
        mScrollable = createScrollable();

        mFab = findViewById(R.id.fab);
        mFabMargin = getResources().getDimensionPixelSize(R.dimen.margin_standard);


        intent_params_parallax = getIntent().getStringArrayExtra(Utils.EXTRA_PARAM_PARALLAX);

        if (intent_params_parallax != null) {

            String MOVIE_TYPE = intent_params_parallax[0];
            itemType = Utils.getItemType(MOVIE_TYPE);
            int position = Integer.parseInt(intent_params_parallax[1]);
//            Toast.makeText(this,"POSS"+intent_params_parallax[1],Toast.LENGTH_SHORT).show();
            movie = Utils.getMovieListIntance(MOVIE_TYPE).get(position);
        }

        mInterceptionLayout = (TouchInterceptionFrameLayout) findViewById(R.id.scroll_wrapper);
        mInterceptionLayout.setScrollInterceptionListener(mInterceptionListener);
        mTitle = (TextView) findViewById(R.id.title);
        String movieTitle = movie.getTitle() + String.format(" ( %s )", movie.getYear());
        mTitle.setText(movieTitle);
        mToolbarTitle = (TextView) findViewById(R.id.toolbar_title);
        mToolbarTitle.setText(getTitle());
        ViewHelper.setAlpha(mToolbarTitle, 0);
        ViewHelper.setTranslationY(mTitle, (mHeaderBarHeight - mActionBarSize) / 2);

        ImageView imageTop = (ImageView) findViewById(R.id.imageTop);
        Glide.with(this)
                .load(movie.getPosters().getThumbnail().replaceAll("tmb.jpg", "det.jpg"))
                .placeholder(R.drawable.gradient_subtitle)
                .centerCrop()
                .crossFade()
//                    .preload(imgWidthDP,imgHeightDP)
                .into(imageTop);
        ImageView trailerView = (ImageView) findViewById(R.id.det_trailerView);


        getNsetTrailerinBackground(this, movieTitle, trailerView);


        ImageView det_critic_img = (ImageView) findViewById(R.id.det_critic_img);
        det_critic_img.setImageResource(getImageResourceForCritic(movie.getRatings().getCritics_rating()));

        TextView det_critic_percentage = (TextView) findViewById(R.id.det_critic_percentage);
        TextView det_critic_ratings = (TextView) findViewById(R.id.det_critic_ratings);
        if (movie.getRatings().getCritics_score().isEmpty() || movie.getRatings().getCritics_score().equals("-1")) {
            det_critic_percentage.setText("");

            det_critic_ratings.setText("Not Yet");

        } else {
            det_critic_percentage.setText(movie.getRatings().getCritics_score() + "%");

            det_critic_ratings.setText(movie.getRatings().getCritics_rating());

        }

        ProgressBar det_critic_progressBar = (ProgressBar) findViewById(R.id.det_critic_progressBar);
        animateProgressBar(det_critic_progressBar, movie.getRatings().getCritics_score(), movie.getRatings().getCritics_rating());

        ImageView det_audi_img = (ImageView) findViewById(R.id.det_audi_img);
        det_audi_img.setImageResource(getImageResourceForAudience(movie.getRatings().getAudience_rating()));

        TextView det_audi_percentage = (TextView) findViewById(R.id.det_audi_percentage);

        TextView det_audi_liked_watch = (TextView) findViewById(R.id.det_audi_liked_watch);
        String like_watch = (movie.getRatings().getAudience_rating().equals("")
                || movie.getRatings().getAudience_rating().equals("-1"))
                ? "want to watch" : "liked it";
//        det_audi_liked_watch.setText(like_watch);
        det_audi_liked_watch.setText("");
        det_audi_percentage.setText(movie.getRatings().getAudience_score() + "% " + like_watch);

        TextView det_synopsis = (TextView) findViewById(R.id.det_synopsis);
        det_synopsis.setText(movie.getSynopsis());

        TextView det_release_date_theaters = (TextView) findViewById(R.id.det_release_date_theaters);
        String date_theater = (movie.getRelease_dates().getTheater().equals("")
                || movie.getRelease_dates().getTheater().equals("-1"))
                ? "Not Yet" : movie.getRelease_dates().getTheater();
        det_release_date_theaters.setText(det_release_date_theaters.getText().toString() + date_theater);

        TextView det_release_date_dvd = (TextView) findViewById(R.id.det_release_date_dvd);
        String date_dvd = (movie.getRelease_dates().getDvd().equals("")
                || movie.getRelease_dates().getDvd().equals("-1"))
                ? "Not Yet" : movie.getRelease_dates().getDvd();
        det_release_date_dvd.setText(det_release_date_dvd.getText().toString() + date_dvd);

        TextView det_runtime = (TextView) findViewById(R.id.det_runtime);

        int time = -1;
        try {
            time = Integer.parseInt(movie.getRuntime());
        } catch (NumberFormatException ex) {
            time = -1;
        }

        if (time != -1)
            det_runtime.setText(String.format("%d hr. %d min.", time / 60, time % 60));
        else
            det_runtime.setText("Not Yet");


        addCharacters_in_CastView(movie.getAbridged_cast());


        if (savedInstanceState == null) {
            mSlidingState = SLIDING_STATE_MIDDLE;
        }

        ScrollUtils.addOnGlobalLayoutListener(mInterceptionLayout, new Runnable() {
            @Override
            public void run() {
                if (mFab != null) {
                    ViewHelper.setTranslationX(mFab, mTitle.getWidth() - mFabMargin - mFab.getWidth());
                    ViewHelper.setTranslationY(mFab, ViewHelper.getX(mTitle) - (mFab.getHeight() / 2));
                }
                changeSlidingState(mSlidingState, false);
            }
        });
    }

    private void getNsetTrailerinBackground(final Context context, final String movieTitle, final ImageView trailerView) {


        if (movie.getVideoThumbnail().isEmpty()) {
//        if (true) {
//
            GSonRequest youtubeReq = new GSonRequest(URL.YOUTUBE_SEARCH_URL(movieTitle), YoutubeQuery.class, null,
                    new Response.Listener() {
                        @Override
                        public void onResponse(Object response) {
                            if (response instanceof YoutubeQuery) {

                                if (((YoutubeQuery) response).getItems().isEmpty())
                                    return;

                                movie.setVideoThumbnail(((YoutubeQuery) response).getItems().get(0).getSnippet().getThumbnails().getMedium().getUrl());
                                movie.setVideoID(((YoutubeQuery) response).getItems().get(0).getId().getVideoId());


                                loadImageViewTrailer(context, movie.getVideoThumbnail(), movie.getVideoID(), trailerView);

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(context, "Unable to fetch Trailer", Toast.LENGTH_SHORT).show();
                        }
                    }
            );


            MyApplication.getInstance().addToRequestQueue(youtubeReq, TAG);


//            thread = new Thread() {
//                public void run() {
//                    YoutubeConnector yc = new YoutubeConnector(context);
//                    final List<VideoItem> searchResults = yc.search(movieTitle);
//                    handler.post(new Runnable() {
//                        public void run() {
//                            if (searchResults == null || Thread.currentThread().isInterrupted())
//                                return;
//
//                            trailer_thumb_url = searchResults.get(0).getThumbnailURL();
//                            trailer_video_url = searchResults.get(0).getId();
//
//                            loadImageViewTrailer(context, trailer_thumb_url, trailer_video_url, trailerView);
//
//                        }
//                    });
//                }
//            };
//
//
//            thread.start();


        } else {


//            Toast.makeText(context, trailer_thumb_url + "----in else", Toast.LENGTH_SHORT);

            loadImageViewTrailer(context, movie.getVideoThumbnail(), movie.getVideoID(), trailerView);
        }
    }

    private void loadImageViewTrailer(final Context context, String trailer_thumb_url, final String trailer_video_url, ImageView trailerView) {


//        Toast.makeText(context, trailer_thumb_url + "----inLoadImageView", Toast.LENGTH_SHORT);

//        Glide.with(this)
//                .load(trailer_thumb_url)
//
//                .placeholder(R.drawable.gradient_subtitle)
//
//                .centerCrop()
//                .crossFade()
////                    .preload(imgWidthDP,imgHeightDP)
//                .into(trailerView);


        Glide.with(this)
                .load(trailer_thumb_url)
                .placeholder(R.drawable.gradient_subtitle_deep)
                .crossFade()
                .centerCrop()
                .into(new ViewTarget<ImageView, GlideDrawable>(trailerView) {
                    @Override
                    public void onResourceReady(GlideDrawable resource, GlideAnimation anim) {
                        ImageView myView = this.view;
                        myView.setBackgroundDrawable(resource);
                        myView.setImageResource(R.drawable.gradient_subtitle_deep);
                    }
                });

        if (trailerView != null) {

            View view_overlay = findViewById(R.id.det_trailerView_overlay);
            view_overlay.setVisibility(View.VISIBLE);
            view_overlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, PlayerActivity.class);
                    intent.putExtra("VIDEO_ID", trailer_video_url);
                    startActivity(intent);
                }
            });

            trailerView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, PlayerActivity.class);
                    intent.putExtra("VIDEO_ID", trailer_video_url);
                    startActivity(intent);
                }
            });
        }


    }

    @Override
    protected void onStop() {

        MyApplication.getInstance().cancelPendingRequests(TAG);

        if (handler != null)
            handler.removeCallbacksAndMessages(null);
        if (thread != null)
            thread.interrupt();
        super.onStop();
    }


    private void addCharacters_in_CastView(List<Abridged_cast> abridged_cast) {


        LinearLayout castViewLayout = (LinearLayout) findViewById(R.id.cast_view_layout);
        //one time initialization
        LinearLayout.LayoutParams wrap_param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        //if no cast character found
        if (abridged_cast.isEmpty()) {

            TextView emptycast = new TextView(this);
            emptycast.setText("No Character Name Found");

            emptycast.setLayoutParams(wrap_param);
            castViewLayout.addView(emptycast);
        } else {

            //iterate through all cast characters
            LinearLayout layout_row = null;
            for (int i = 0; i < abridged_cast.size(); i++) {

                if (((i + 1) % 2) != 0) {


                    //if 1st column or odd column create new linear layout
                    layout_row = new LinearLayout(this);
                    layout_row.setLayoutParams(wrap_param);
                    layout_row.setOrientation(LinearLayout.HORIZONTAL);


                    View cast_view_single = getLayoutInflater().inflate(R.layout.cast_view_row, layout_row, false);

                    //add cast character image later
                    ImageView cast_imView = (ImageView) cast_view_single.findViewById(R.id.det_cast_1_imView);

                    TextView cast_txtView = (TextView) cast_view_single.findViewById(R.id.det_cast_1);
                    cast_txtView.setText(movie.getAbridged_cast().get(i).getName());
                    TextView cast_txtView_character = (TextView) cast_view_single.findViewById(R.id.det_cast_1_character);
                    if (!movie.getAbridged_cast().get(i).getCharacters().isEmpty())
                        cast_txtView_character.setText(movie.getAbridged_cast().get(i).getCharacters().get(0));
                    else
                        cast_txtView_character.setText("");

                    layout_row.addView(cast_view_single);

                } else {
                    View cast_view_single = getLayoutInflater().inflate(R.layout.cast_view_row, layout_row, false);
                    //add cast character image later
                    ImageView cast_imView = (ImageView) cast_view_single.findViewById(R.id.det_cast_1_imView);

                    TextView cast_txtView = (TextView) cast_view_single.findViewById(R.id.det_cast_1);
                    cast_txtView.setText(movie.getAbridged_cast().get(i).getName());
                    TextView cast_txtView_character = (TextView) cast_view_single.findViewById(R.id.det_cast_1_character);
                    if (!movie.getAbridged_cast().get(i).getCharacters().isEmpty())
                        cast_txtView_character.setText(movie.getAbridged_cast().get(i).getCharacters().get(0));
                    else
                        cast_txtView_character.setText("");

                    layout_row.addView(cast_view_single);


                    castViewLayout.addView(layout_row);

                }


            }


            //for odd number of characters add last view
            if (((abridged_cast.size()) % 2) != 0) {
                castViewLayout.addView(layout_row);
//                castViewLayout.invalidate();
            }


        }


    }

    private void animateProgressBar(ProgressBar det_critic_progressBar, String critics_score, String critics_rating) {
        int score_progress;

        if (critics_score.equals("-1") || critics_score.equals(""))
            score_progress = 1;
        else
            score_progress = Integer.parseInt(critics_score);

        if (critics_rating.equals("Certified Fresh") || critics_rating.equals("Fresh"))
            det_critic_progressBar.setProgressDrawable(getResources().getDrawable(R.drawable.custom_progress_bar_horizontal_red));
        else
            det_critic_progressBar.setProgressDrawable(getResources().getDrawable(R.drawable.custom_progress_bar_horizontal_green));


        ObjectAnimator animation = ObjectAnimator.ofInt(det_critic_progressBar, "progress", 0, score_progress);
        animation.setDuration(1300);
        animation.setInterpolator(new DecelerateInterpolator());
        animation.start();

    }


    private int getImageResourceForCritic(String itemType) {


        int resId;

        if (itemType.equals("") || itemType.equals("-1")) {
            resId = android.R.color.transparent;

        } else if (itemType.equals("Certified Fresh")) {
            resId = R.drawable.certified_fresh;

        } else if (itemType.equals("Fresh")) {
            resId = R.drawable.fresh;
        } else if (itemType.equals("Rotten")) {
            resId = R.drawable.rotten;
        } else {
            resId = -1;
        }

        return resId;

    }

    private int getImageResourceForAudience(String itemType) {
        int resId;

        if (itemType.equals("") || itemType.equals("-1")) {
            resId = R.drawable.addlist;


        } else if (itemType.equals("Spilled")) {
            resId = R.drawable.spilt;

        } else if (itemType.equals("Upright")) {
            resId = R.drawable.popcorn;
        } else {
            resId = -1;
        }

        return resId;
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // All the related temporary states can be restored by slidingState
        mSlidingState = savedInstanceState.getInt(STATE_SLIDING_STATE);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(STATE_SLIDING_STATE, mSlidingState);
        super.onSaveInstanceState(outState);
    }

    protected abstract int getLayoutResId();

    protected abstract S createScrollable();

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
    }

    @Override
    public void onDownMotionEvent() {
    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
    }

    private TouchInterceptionFrameLayout.TouchInterceptionListener mInterceptionListener = new TouchInterceptionFrameLayout.TouchInterceptionListener() {
        @Override
        public boolean shouldInterceptTouchEvent(MotionEvent ev, boolean moving, float diffX, float diffY) {
            final int minInterceptionLayoutY = -mIntersectionHeight;
            return minInterceptionLayoutY < (int) ViewHelper.getY(mInterceptionLayout)
                    || (moving && mScrollable.getCurrentScrollY() - diffY < 0);
        }

        @Override
        public void onDownMotionEvent(MotionEvent ev) {
            mScrollYOnDownMotion = mScrollable.getCurrentScrollY();
            mInitialY = ViewHelper.getTranslationY(mInterceptionLayout);
        }

        @Override
        public void onMoveMotionEvent(MotionEvent ev, float diffX, float diffY) {
            mMoved = true;
            float translationY = ViewHelper.getTranslationY(mInterceptionLayout) - mScrollYOnDownMotion + diffY;
            if (translationY < -mIntersectionHeight) {
                translationY = -mIntersectionHeight;
            } else if (getScreenHeight() - mHeaderBarHeight < translationY) {
                translationY = getScreenHeight() - mHeaderBarHeight;
            }

            slideTo(translationY, true);

            mMovedDistanceY = ViewHelper.getTranslationY(mInterceptionLayout) - mInitialY;
        }

        @Override
        public void onUpOrCancelMotionEvent(MotionEvent ev) {
            if (!mMoved) {
                // Invoke slide animation only on header view
                Rect outRect = new Rect();
                mHeader.getHitRect(outRect);
                if (outRect.contains((int) ev.getX(), (int) ev.getY())) {
                    slideOnClick();
                }
            } else {
                stickToAnchors();
            }
            mMoved = false;
        }
    };

    private void changeSlidingState(final int slidingState, boolean animated) {
        mSlidingState = slidingState;
        float translationY = 0;
        switch (slidingState) {
            case SLIDING_STATE_TOP:
                translationY = 0;
                break;
            case SLIDING_STATE_MIDDLE:
                translationY = getAnchorYImage();
                break;
            case SLIDING_STATE_BOTTOM:
                translationY = getAnchorYBottom();
                break;
        }
        if (animated) {
            slideWithAnimation(translationY);
        } else {
            slideTo(translationY, false);
        }
    }

    private void slideOnClick() {
        float translationY = ViewHelper.getTranslationY(mInterceptionLayout);
        if (translationY == getAnchorYBottom()) {
            changeSlidingState(SLIDING_STATE_MIDDLE, true);
        } else if (translationY == getAnchorYImage()) {
            changeSlidingState(SLIDING_STATE_BOTTOM, true);
        }
    }

    private void stickToAnchors() {
        // Slide to some points automatically
        if (0 < mMovedDistanceY) {
            // Sliding down
            if (mSlidingSlop < mMovedDistanceY) {
                // Sliding down to an anchor
                if (getAnchorYImage() < ViewHelper.getTranslationY(mInterceptionLayout)) {
                    changeSlidingState(SLIDING_STATE_BOTTOM, true);
                } else {
                    changeSlidingState(SLIDING_STATE_MIDDLE, true);
                }
            } else {
                // Sliding up(back) to an anchor
                if (getAnchorYImage() < ViewHelper.getTranslationY(mInterceptionLayout)) {
                    changeSlidingState(SLIDING_STATE_MIDDLE, true);
                } else {
                    changeSlidingState(SLIDING_STATE_TOP, true);
                }
            }
        } else if (mMovedDistanceY < 0) {
            // Sliding up
            if (mMovedDistanceY < -mSlidingSlop) {
                // Sliding up to an anchor
                if (getAnchorYImage() < ViewHelper.getTranslationY(mInterceptionLayout)) {
                    changeSlidingState(SLIDING_STATE_MIDDLE, true);
                } else {
                    changeSlidingState(SLIDING_STATE_TOP, true);
                }
            } else {
                // Sliding down(back) to an anchor
                if (getAnchorYImage() < ViewHelper.getTranslationY(mInterceptionLayout)) {
                    changeSlidingState(SLIDING_STATE_BOTTOM, true);
                } else {
                    changeSlidingState(SLIDING_STATE_MIDDLE, true);
                }
            }
        }
    }

    private void slideTo(float translationY, final boolean animated) {
        ViewHelper.setTranslationY(mInterceptionLayout, translationY);

        if (translationY < 0) {
            FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) mInterceptionLayout.getLayoutParams();
            lp.height = (int) -translationY + getScreenHeight();
            mInterceptionLayout.requestLayout();
        }

        // Translate title
        float hiddenHeight = translationY < 0 ? -translationY : 0;
        ViewHelper.setTranslationY(mTitle, Math.min(mIntersectionHeight, (mHeaderBarHeight + hiddenHeight - mActionBarSize) / 2));

        // Translate image
        float imageAnimatableHeight = getScreenHeight() - mHeaderBarHeight;
        float imageTranslationScale = imageAnimatableHeight / (imageAnimatableHeight - mImageView.getHeight());
        float imageTranslationY = Math.max(0, imageAnimatableHeight - (imageAnimatableHeight - translationY) * imageTranslationScale);
        ViewHelper.setTranslationY(mImageView, imageTranslationY);

        // Show/hide FAB
        if (ViewHelper.getTranslationY(mInterceptionLayout) < mFlexibleSpaceImageHeight) {
            hideFab(animated);
        } else {
            if (animated) {
                ViewPropertyAnimator.animate(mToolbar).scaleY(0).setDuration(200).start();
            } else {
                ViewHelper.setScaleY(mToolbar, 0);
            }
            showFab(animated);
        }
        if (ViewHelper.getTranslationY(mInterceptionLayout) <= mFlexibleSpaceImageHeight) {
            if (animated) {
                ViewPropertyAnimator.animate(mToolbar).scaleY(1).setDuration(200).start();
            } else {
                ViewHelper.setScaleY(mToolbar, 1);
            }
            mToolbar.setBackgroundColor(ScrollUtils.getColorWithAlpha(0, mToolbarColor));
        }

        changeToolbarTitleVisibility();
        changeHeaderBarColorAnimated(animated);
        changeHeaderOverlay();
    }

    private void slideWithAnimation(float toY) {
        float layoutTranslationY = ViewHelper.getTranslationY(mInterceptionLayout);
        if (layoutTranslationY != toY) {
            ValueAnimator animator = ValueAnimator.ofFloat(ViewHelper.getTranslationY(mInterceptionLayout), toY).setDuration(200);
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    slideTo((float) Double.parseDouble(animation.getAnimatedValue().toString()), true);
                }
            });
            animator.start();
        }
    }

    private void changeToolbarTitleVisibility() {
        if (ViewHelper.getTranslationY(mInterceptionLayout) <= mIntersectionHeight) {
            if (ViewHelper.getAlpha(mToolbarTitle) != 1) {
                ViewPropertyAnimator.animate(mToolbarTitle).cancel();
                ViewPropertyAnimator.animate(mToolbarTitle).alpha(1).setDuration(200).start();
            }
        } else if (ViewHelper.getAlpha(mToolbarTitle) != 0) {
            ViewPropertyAnimator.animate(mToolbarTitle).cancel();
            ViewPropertyAnimator.animate(mToolbarTitle).alpha(0).setDuration(200).start();
        } else {
            ViewHelper.setAlpha(mToolbarTitle, 0);
        }
    }

    private void changeHeaderBarColorAnimated(boolean animated) {
        if (mHeaderColorIsChanging) {
            return;
        }
        boolean shouldBeWhite = getAnchorYBottom() == ViewHelper.getTranslationY(mInterceptionLayout);
        if (!mHeaderIsAtBottom && !mHeaderColorChangedToBottom && shouldBeWhite) {
            mHeaderIsAtBottom = true;
            mHeaderIsNotAtBottom = false;
            if (animated) {
                ValueAnimator animator = ValueAnimator.ofFloat(0, 1).setDuration(100);
                animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        float alpha = (float) Double.parseDouble(animation.getAnimatedValue().toString());
                        mHeaderColorIsChanging = (alpha != 1);
                        changeHeaderBarColor(alpha);
                    }
                });
                animator.start();
            } else {
                changeHeaderBarColor(1);
            }
        } else if (!mHeaderIsNotAtBottom && !shouldBeWhite) {
            mHeaderIsAtBottom = false;
            mHeaderIsNotAtBottom = true;
            if (animated) {
                ValueAnimator animator = ValueAnimator.ofFloat(1, 0).setDuration(100);
                animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        float alpha = (float) Double.parseDouble(animation.getAnimatedValue().toString());
                        mHeaderColorIsChanging = (alpha != 0);
                        changeHeaderBarColor(alpha);
                    }
                });
                animator.start();
            } else {
                changeHeaderBarColor(0);
            }
        }
    }

    private void changeHeaderBarColor(float alpha) {
        mHeaderBar.setBackgroundColor(ScrollUtils.mixColors(mColorPrimary, Color.WHITE, alpha));
        mTitle.setTextColor(ScrollUtils.mixColors(Color.WHITE, Color.BLACK, alpha));
        mHeaderColorChangedToBottom = (alpha == 1);
    }

    private void changeHeaderOverlay() {
        final float translationY = ViewHelper.getTranslationY(mInterceptionLayout);
        if (translationY <= mToolbar.getHeight() - mSlidingHeaderBlueSize) {
            mHeaderOverlay.setVisibility(View.VISIBLE);
            mHeaderFlexibleSpace.getLayoutParams().height = (int) (mToolbar.getHeight() - mSlidingHeaderBlueSize - translationY);
            mHeaderFlexibleSpace.requestLayout();
            mHeaderOverlay.requestLayout();
        } else {
            mHeaderOverlay.setVisibility(View.INVISIBLE);
        }
    }

    private void showFab(boolean animated) {
        if (mFab == null) {
            return;
        }
        if (!mFabIsShown) {
            if (animated) {
                ViewPropertyAnimator.animate(mFab).cancel();
                ViewPropertyAnimator.animate(mFab).scaleX(1).scaleY(1).setDuration(200).start();
            } else {
                ViewHelper.setScaleX(mFab, 1);
                ViewHelper.setScaleY(mFab, 1);
            }
            mFabIsShown = true;
        } else {
            // Ensure that FAB is shown
            ViewHelper.setScaleX(mFab, 1);
            ViewHelper.setScaleY(mFab, 1);
        }
    }

    private void hideFab(boolean animated) {
        if (mFab == null) {
            return;
        }
        if (mFabIsShown) {
            if (animated) {
                ViewPropertyAnimator.animate(mFab).cancel();
                ViewPropertyAnimator.animate(mFab).scaleX(0).scaleY(0).setDuration(200).start();
            } else {
                ViewHelper.setScaleX(mFab, 0);
                ViewHelper.setScaleY(mFab, 0);
            }
            mFabIsShown = false;
        } else {
            // Ensure that FAB is hidden
            ViewHelper.setScaleX(mFab, 0);
            ViewHelper.setScaleY(mFab, 0);
        }
    }

    private float getAnchorYBottom() {
        return getScreenHeight() - mHeaderBarHeight;
    }

    private float getAnchorYImage() {
        return mImageView.getHeight();
    }
}
