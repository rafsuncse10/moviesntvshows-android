package me.rafsun.moviesntvshows.ui;

import android.app.Application;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.integration.volley.VolleyUrlLoader;
import com.bumptech.glide.load.model.GlideUrl;

import java.io.InputStream;

/**
 * Created by Rafsun on 1/16/15.
 */
public class MyApplication extends Application {

    public static final String TAG = MyApplication.class.getSimpleName();


    private RequestQueue mRequestQueue;
    private boolean isParallaxActivityCalled;

    private ImageLoader mImageLoader;

    public boolean isParallaxActivityCalled() {
        return isParallaxActivityCalled;
    }

    public void setParallaxActivityCalled(boolean isParallaxActivityCalled) {
        this.isParallaxActivityCalled = isParallaxActivityCalled;
    }
//    private int FirstBox ;
//    private int FirstUpcoming;
//    private int FirstInTheater;
//    private int FirstOpening;

    private static MyApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        registerGlideWithVolley();
    }

    private void registerGlideWithVolley() {

        Glide.get(getApplicationContext()).register(GlideUrl.class, InputStream.class,
                new VolleyUrlLoader.Factory(getRequestQueue()));
    }


    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());

        }

        return mRequestQueue;
    }

//    public boolean isFirst(ItemType itemType) {
//
//        switch (itemType) {
//            case IN_THEATER:
//                FirstInTheater = FirstInTheater == -1 ? 1 : 0;
//                return FirstInTheater == 1 ? true : false;
//            case UPCOMING:
//                FirstUpcoming = FirstUpcoming == -1 ? 1 : 0;
//                return FirstUpcoming == 1 ? true : false;
//            case OPENING:
//                FirstOpening = FirstOpening == -1 ? 1 : 0;
//                return FirstOpening == 1 ? true : false;
//            case BOX_OFFICE:
//                FirstBox = FirstBox == -1 ? 1 : 0;
//                return FirstBox == 1 ? true : false;
//        }
//        return false;
//    }



    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache(getApplicationContext()));
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public void cancelPendingRequests() {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(TAG);
        }
    }


}
