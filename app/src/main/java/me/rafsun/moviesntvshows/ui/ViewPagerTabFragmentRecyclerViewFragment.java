/*
 * Copyright 2014 Soichiro Kashima
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.rafsun.moviesntvshows.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView;

import java.util.List;

import me.rafsun.moviesntvshows.R;
import me.rafsun.moviesntvshows.Singletons.SingletonBoxOffice;
import me.rafsun.moviesntvshows.Singletons.SingletonInTheaters;
import me.rafsun.moviesntvshows.Singletons.SingletonOpening;
import me.rafsun.moviesntvshows.Singletons.SingletonUpcoming;
import me.rafsun.moviesntvshows.json.box.Movie;
import me.rafsun.moviesntvshows.util.ItemType;
import me.rafsun.moviesntvshows.util.Utils;


/**
 * Fragment for ViewPagerTabFragmentActivity.
 * ScrollView callbacks are handled by its parent fragment, not its parent activity.
 */
public class ViewPagerTabFragmentRecyclerViewFragment extends BaseFragment {
    private static String MOVIE_DATA_EXTRA = "MovieData";
    private ItemType mItemType ;
    private SimpleRecyclerAdapter adapter;
    private List<Movie> movies;
    private SingletonBoxOffice singletonBoxOffice ;
    private SingletonInTheaters singletonInTheaters;
    private SingletonOpening singletonOpening;
    private SingletonUpcoming singletonUpcoming;


    public static ViewPagerTabFragmentRecyclerViewFragment newInstance(ItemType movieType) {
        final ViewPagerTabFragmentRecyclerViewFragment f = new ViewPagerTabFragmentRecyclerViewFragment();

        final Bundle args = new Bundle();
        args.putString(MOVIE_DATA_EXTRA, movieType.movie());
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        String MOVIE_TYPE = getArguments() != null ? getArguments().getString(MOVIE_DATA_EXTRA) : null;
        //get movie type and initialize singletone
        mItemType = Utils.getItemType(MOVIE_TYPE);
        movies = Utils.getMovieListIntance(MOVIE_TYPE);

        adapter = new SimpleRecyclerAdapter(getActivity(),mItemType,movies);
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recyclerview, container, false);


        //////////////////////////////
        //  Setup Swipe To Refresh  //
        //////////////////////////////
        final SwipeRefreshLayout swiper = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        swiper.setSize(SwipeRefreshLayout.LARGE);
        swiper.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_orange_light,
                android.R.color.holo_green_light,
                android.R.color.holo_red_light
        );

//        swiper.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                // This is where you should kickoff the
//                // refreshing task.
//
//                // For now, just wait a few seconds and turn off refreshing.
//                new Handler().postDelayed(new Runnable() {
//                    @Override public void run() {
////                        if (myDataset != null && mAdapter != null) {
////                            Collections.shuffle(myDataset);
////                            mAdapter.notifyDataSetChanged();
////                        }
//                        swiper.setRefreshing(false);
//                    }
//                }, 2000);
//            }
//        });


        final ObservableRecyclerView recyclerView = (ObservableRecyclerView) view.findViewById(R.id.scroll);
        final StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);



        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        recyclerView.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        recyclerView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        int viewWidth = recyclerView.getMeasuredWidth();
                        float columnwidth = getResources().getDimension(R.dimen.column_width);
                        int newSpanCount = (int) Math.floor(viewWidth / columnwidth);
                        staggeredGridLayoutManager.setSpanCount(newSpanCount);
                        staggeredGridLayoutManager.requestLayout();
                    }
                });


        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        recyclerView.setHasFixedSize(true);

//        populateAdapter(mItemType,recyclerView);
//        populateAdapter(mItemType,movies,adapter,recyclerView);
        populateAdapter(mItemType,movies,adapter,recyclerView,swiper);




//        recyclerView.setOnScrollListener();



        Fragment parentFragment = getParentFragment();
//        RecyclerView recyclerView1 = new RecyclerView(getActivity());


        ViewGroup viewGroup = (ViewGroup) parentFragment.getView();
        if (viewGroup != null) {
            recyclerView.setTouchInterceptionViewGroup((ViewGroup) viewGroup.findViewById(R.id.container));


//            if (parentFragment instanceof ObservableScrollViewCallbacks) {
//                recyclerView.setScrollViewCallbacks((ObservableScrollViewCallbacks) parentFragment);
//            }
            recyclerView.setOnScrollListener(new ObservableRecyclerView.OnScrollListener(){


                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    // This 'magic' came from: http://stackoverflow.com/a/25183693/2259418
                    // Needed to make sure refresh only occurs from TOP of list.
                    int topRowVerticalPosition =
                            (recyclerView == null || recyclerView.getChildCount() == 0)
                                    ? 0 : recyclerView.getChildAt(0).getTop();
                    swiper.setEnabled(topRowVerticalPosition >= 0);
                    super.onScrolled(recyclerView, dx, dy);
                }
            });



        }
        return view;
    }

    //    }
//
//
//        }
//            mImageFetcher.loadImage(mImageUrl, mImageView);
//            mImageFetcher = ((ImageDetailActivity) getActivity()).getImageFetcher();
//        if (ImageDetailActivity.class.isInstance(getActivity())) {
//        // cache can be used over all pages in the ViewPager
//        // Use the parent activity to load the image asynchronously into the ImageView (so a single
//
//        super.onActivityCreated(savedInstanceState);
//    public void onActivityCreated(Bundle savedInstanceState) {






}
