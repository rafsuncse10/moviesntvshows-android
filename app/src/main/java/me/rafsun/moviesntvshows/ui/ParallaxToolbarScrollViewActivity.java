/*
 * Copyright 2014 Soichiro Kashima
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.rafsun.moviesntvshows.ui;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.nineoldandroids.view.ViewHelper;

import me.rafsun.moviesntvshows.R;
import me.rafsun.moviesntvshows.json.box.Movie;
import me.rafsun.moviesntvshows.util.ItemType;
import me.rafsun.moviesntvshows.util.Utils;


public class ParallaxToolbarScrollViewActivity extends BaseActivity implements ObservableScrollViewCallbacks {

    private ImageView mImageView;
    private View mToolbarView;
    private ObservableScrollView mScrollView;
    private int mParallaxImageHeight;
    private Movie movie ;
    private String intent_params_parallax[];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parallaxtoolbarscrollview);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        intent_params_parallax = getIntent().getStringArrayExtra(Utils.EXTRA_PARAM_PARALLAX);

        if(intent_params_parallax != null){

            String MOVIE_TYPE = intent_params_parallax[0];
            ItemType curr_type = Utils.getItemType(MOVIE_TYPE);
            int position = Integer.parseInt(intent_params_parallax[1]);
//            Toast.makeText(this,"POSS"+intent_params_parallax[1],Toast.LENGTH_SHORT).show();
            movie = Utils.getMovieListIntance(MOVIE_TYPE).get(position);
        }


        mImageView = (ImageView)findViewById(R.id.image);

        Glide.with(this)
                .load(movie.getPosters().getDetailed().replaceAll("tmb.jpg","det.jpg") )
//                .placeholder(R.drawable.example)
                .centerCrop()
                .crossFade()
//                    .preload(imgWidthDP,imgHeightDP)
                .into(mImageView);

//        mImageView.setImageResource(R.drawable.example);

        TextView textView = (TextView) findViewById(R.id.body);
        textView.setText(movie.toString());

        mToolbarView = findViewById(R.id.toolbar);
        mToolbarView.setBackgroundColor(ScrollUtils.getColorWithAlpha(0, getResources().getColor(R.color.primary)));

        mScrollView = (ObservableScrollView) findViewById(R.id.scroll);
        mScrollView.setScrollViewCallbacks(this);

        mParallaxImageHeight = getResources().getDimensionPixelSize(R.dimen.parallax_image_height);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        onScrollChanged(mScrollView.getCurrentScrollY(), false, false);
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        int baseColor = getResources().getColor(R.color.primary);
        float alpha = 1 - (float) Math.max(0, mParallaxImageHeight - scrollY) / mParallaxImageHeight;
        mToolbarView.setBackgroundColor(ScrollUtils.getColorWithAlpha(alpha, baseColor));
        ViewHelper.setTranslationY(mImageView, scrollY / 2);
    }

    @Override
    public void onDownMotionEvent() {
    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
    }
}
