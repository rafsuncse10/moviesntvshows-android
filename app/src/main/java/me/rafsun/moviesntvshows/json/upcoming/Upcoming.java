
package me.rafsun.moviesntvshows.json.upcoming;


import java.util.ArrayList;
import java.util.List;

import me.rafsun.moviesntvshows.json.box.Links;
import me.rafsun.moviesntvshows.json.box.Movie;

public class Upcoming{
	private String total = "";
	private List<Movie> movies = new ArrayList<Movie>();
	private Links links;
	private String link_template = "";

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public List<Movie> getMovies() {
		return movies;
	}

	public void setMovies(List<Movie> movies) {
		this.movies = movies;
	}

	public Links getLinks() {
		return links;
	}

	public void setLinks(Links links) {
		this.links = links;
	}

	public String getLink_template() {
		return link_template;
	}

	public void setLink_template(String link_template) {
		this.link_template = link_template;
	}

	@Override
	public String toString() {
		return "Upcoming{" +
				"total=" + total +
				", movies=" + movies +
				", links=" + links +
				", link_template='" + link_template + '\'' +
				'}';
	}
}
