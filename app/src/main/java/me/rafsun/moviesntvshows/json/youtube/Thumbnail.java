package me.rafsun.moviesntvshows.json.youtube;

/**
 * Created by Rafsun on 2/6/15.
 */
public class Thumbnail {

    Medium medium;
    High high;

    public Medium getMedium() {
        return medium;
    }

    public void setMedium(Medium medium) {
        this.medium = medium;
    }

    public High getHigh() {
        return high;
    }

    public void setHigh(High high) {
        this.high = high;
    }


    @Override
    public String toString() {
        return "Thumbnail{" +
                "medium=" + medium +
                ", high=" + high +
                '}';
    }
}
