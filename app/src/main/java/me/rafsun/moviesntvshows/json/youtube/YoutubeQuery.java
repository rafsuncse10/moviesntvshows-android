package me.rafsun.moviesntvshows.json.youtube;

import java.util.List;

/**
 * Created by Rafsun on 2/6/15.
 */
public class YoutubeQuery {
    List<Items> items;

    public List<Items> getItems() {
        return items;
    }

    public void setItems(List<Items> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "YoutubeQuery{" +
                "items=" + items +
                '}';
    }
}
