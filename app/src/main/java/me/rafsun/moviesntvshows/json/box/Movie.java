package me.rafsun.moviesntvshows.json.box;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rafsun on 1/13/15.
 */
public class Movie {


    private String id="";
    private String title="";
    private String year="";
    private String mpaa_rating="";
    private String runtime="";
    private String critics_consensus="";
    private Release_dates release_dates;
    private Ratings ratings;
    private String synopsis="";
    private Posters posters;
    private List<Abridged_cast> abridged_cast = new ArrayList<Abridged_cast>();
    private Alternate_ids alternate_ids;
    private Links links= new Links();


    public String getVideoThumbnail() {
        return videoThumbnail;
    }

    public void setVideoThumbnail(String videoThumbnail) {
        this.videoThumbnail = videoThumbnail;
    }

    public String getVideoID() {
        return videoID;
    }

    public void setVideoID(String videoID) {
        this.videoID = videoID;
    }

    private String videoThumbnail="";
    private String videoID="";


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMpaa_rating() {
        return mpaa_rating;
    }

    public void setMpaa_rating(String mpaa_rating) {
        this.mpaa_rating = mpaa_rating;
    }

    public String getRuntime() {
        return runtime;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public String getCritics_consensus() {
        return critics_consensus;
    }

    public void setCritics_consensus(String critics_consensus) {
        this.critics_consensus = critics_consensus;
    }

    public Release_dates getRelease_dates() {
        return release_dates;
    }

    public void setRelease_dates(Release_dates release_dates) {
        this.release_dates = release_dates;
    }

    public Ratings getRatings() {
        return ratings;
    }

    public void setRatings(Ratings ratings) {
        this.ratings = ratings;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public Posters getPosters() {
        return posters;
    }

    public void setPosters(Posters posters) {
        this.posters = posters;
    }

    public List<Abridged_cast> getAbridged_cast() {
        return abridged_cast;
    }

    public void setAbridged_cast(List<Abridged_cast> abridged_cast) {
        this.abridged_cast = abridged_cast;
    }

    public Alternate_ids getAlternate_ids() {
        return alternate_ids;
    }

    public void setAlternate_ids(Alternate_ids alternate_ids) {
        this.alternate_ids = alternate_ids;
    }

    public Links getLinks() {
        return links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", year='" + year + '\'' +
                ", mpaa_rating='" + mpaa_rating + '\'' +
                ", runtime='" + runtime + '\'' +
                ", critics_consensus='" + critics_consensus + '\'' +
                ", release_dates=" + release_dates +
                ", ratings=" + ratings +
                ", synopsis='" + synopsis + '\'' +
                ", posters=" + posters +
                ", abridged_cast=" + abridged_cast +
                ", alternate_ids=" + alternate_ids +
                ", links=" + links +
                ", videoThumbnail='" + videoThumbnail + '\'' +
                ", videoID='" + videoID + '\'' +
                '}';
    }
}
