package me.rafsun.moviesntvshows.json.youtube;

/**
 * Created by Rafsun on 2/6/15.
 */
public class Snippet {

    Thumbnail thumbnails;

    public Thumbnail getThumbnails() {
        return thumbnails;
    }

    public void setThumbnails(Thumbnail thumbnails) {
        this.thumbnails = thumbnails;
    }


    @Override
    public String toString() {
        return "Snippet{" +
                "thumbnails=" + thumbnails +
                '}';
    }
}
