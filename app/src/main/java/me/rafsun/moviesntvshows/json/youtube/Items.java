package me.rafsun.moviesntvshows.json.youtube;

/**
 * Created by Rafsun on 2/6/15.
 */
public class Items {

    Id id;
    Snippet snippet;

    public Id getId() {
        return id;
    }

    public void setId(Id id) {
        this.id = id;
    }

    public Snippet getSnippet() {
        return snippet;
    }

    public void setSnippet(Snippet snippet) {
        this.snippet = snippet;
    }

    @Override
    public String toString() {
        return "Items{" +
                "id=" + id +
                ", snippet=" + snippet +
                '}';
    }
}
