package me.rafsun.moviesntvshows.json.youtube;

/**
 * Created by Rafsun on 2/6/15.
 */
public class Id {

    String videoId;

    public String getVideoId() {
        return videoId;
    }
//    public String getVideoId() {
//        return "https://www.youtube.com/watch?v="+ videoId;
//    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    @Override
    public String toString() {
        return "Id{" +
                "videoId='" + videoId + '\'' +
                '}';
    }
}
