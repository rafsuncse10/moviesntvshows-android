package me.rafsun.moviesntvshows.json.box;

/**
 * Created by Rafsun on 1/12/15.
 */
public class Release_dates {
    private String theater="";
    private String dvd="";


    public String getTheater() {
        return theater;
    }

    public void setTheater(String theater) {
        this.theater = theater;
    }

    public String getDvd() {
        return dvd;
    }

    public void setDvd(String dvd) {
        this.dvd = dvd;
    }

    @Override
    public String toString() {
        return "Release_dates{" +
                "theater='" + theater + '\'' +
                ", dvd='" + dvd + '\'' +
                '}';
    }
}
