
package me.rafsun.moviesntvshows.json.box;

import java.util.List;

public class Alternate_ids{
   	private String imdb="";

 	public String getImdb(){
		return this.imdb;
	}
	public void setImdb(String imdb){
		this.imdb = imdb;
	}

	@Override
	public String toString() {
		return "Alternate_ids{" +
				"imdb='" + imdb + '\'' +
				'}';
	}
}
