package me.rafsun.moviesntvshows.json.youtube;

/**
 * Created by Rafsun on 2/6/15.
 */
public class High {

    String url;

    public String getUrl() {
        return  url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "High{" +
                "url='" + url + '\'' +
                '}';
    }
}
