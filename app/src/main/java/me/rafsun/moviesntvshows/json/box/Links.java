
package me.rafsun.moviesntvshows.json.box;

public class Links{
	private String self = "";
	private String next = "";
	private String alternate = "";
	private String cast = "";
	private String reviews = "";
	private String similar = "";

	public String getSelf() {
		return self;
	}

	public void setSelf(String self) {
		this.self = self;
	}

	public String getNext() {
		return next;
	}

	public void setNext(String next) {
		this.next = next;
	}

	public String getAlternate() {
		return alternate;
	}

	public void setAlternate(String alternate) {
		this.alternate = alternate;
	}

	public String getCast() {
		return cast;
	}

	public void setCast(String cast) {
		this.cast = cast;
	}

	public String getReviews() {
		return reviews;
	}

	public void setReviews(String reviews) {
		this.reviews = reviews;
	}

	public String getSimilar() {
		return similar;
	}

	public void setSimilar(String similar) {
		this.similar = similar;
	}

	@Override
	public String toString() {
		return "Links{" +
				"self='" + self + '\'' +
				", next='" + next + '\'' +
				", alternate='" + alternate + '\'' +
				", cast='" + cast + '\'' +
				", reviews='" + reviews + '\'' +
				", similar='" + similar + '\'' +
				'}';
	}
}
