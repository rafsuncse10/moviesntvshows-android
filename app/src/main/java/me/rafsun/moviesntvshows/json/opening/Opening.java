
package me.rafsun.moviesntvshows.json.opening;


import java.util.ArrayList;
import java.util.List;

import me.rafsun.moviesntvshows.json.box.Links;
import me.rafsun.moviesntvshows.json.box.Movie;

public class Opening{
	private List<Movie> movies = new ArrayList<Movie>();
	private Links links;
	private String link_template = "";


	public List<Movie> getMovies() {
		return movies;
	}

	public void setMovies(List<Movie> movies) {
		this.movies = movies;
	}

	public Links getLinks() {
		return links;
	}

	public void setLinks(Links links) {
		this.links = links;
	}

	public String getLink_template() {
		return link_template;
	}

	public void setLink_template(String link_template) {
		this.link_template = link_template;
	}

	@Override
	public String toString() {
		return "Opening{" +
				"movies=" + movies +
				", links=" + links +
				", link_template='" + link_template + '\'' +
				'}';
	}
}
