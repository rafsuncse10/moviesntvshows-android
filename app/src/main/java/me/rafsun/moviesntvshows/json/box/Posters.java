package me.rafsun.moviesntvshows.json.box;

/**
 * Created by Rafsun on 1/12/15.
 */
public class Posters {

    private String thumbnail="";
    private String profile="";
    private String detailed="";
    private String original="";

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getDetailed() {
        return detailed;
    }

    public void setDetailed(String detailed) {
        this.detailed = detailed;
    }

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }


    @Override
    public String toString() {
        return "Posters{" +
                "thumbnail='" + thumbnail + '\'' +
                ", profile='" + profile + '\'' +
                ", detailed='" + detailed + '\'' +
                ", original='" + original + '\'' +
                '}';
    }
}
