
package me.rafsun.moviesntvshows.json.box;

import java.util.ArrayList;
import java.util.List;

public class BoxOffice{
	private List<Movie> movies = new ArrayList<Movie>();
	private Links links;
	private String link_template = "";

	public List<Movie> getMovies() {
		return movies;
	}

	public void setMovies(List<Movie> movies) {
		this.movies = movies;
	}

	public Links getLinks() {
		return links;
	}

	public void setLinks(Links links) {
		this.links = links;
	}

	public String getLink_template() {
		return link_template;
	}

	public void setLink_template(String link_template) {
		this.link_template = link_template;
	}

	@Override
	public String toString() {
		return "BoxOffice{" +
				"movies=" + movies +
				", links=" + links +
				", link_template='" + link_template + '\'' +
				'}';
	}
}
