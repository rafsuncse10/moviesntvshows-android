
package me.rafsun.moviesntvshows.json.box;

import java.util.ArrayList;
import java.util.List;

public class Abridged_cast{
	private String name = "";
	private String id = "";
	private List<String> characters = new ArrayList<String>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<String> getCharacters() {
		return characters;
	}

	public void setCharacters(List<String> characters) {
		this.characters = characters;
	}


	@Override
	public String toString() {
		return "Abridged_cast{" +
				"name='" + name + '\'' +
				", id='" + id + '\'' +
				", characters=" + characters +
				'}';
	}
}
