package me.rafsun.moviesntvshows.Singletons;

import java.util.ArrayList;
import java.util.List;

import me.rafsun.moviesntvshows.json.box.Movie;


/**
 * Created by Rafsun on 1/19/15.
 */
public enum SingletonOpening {
    INSTANCE;
    private List<Movie> movies = new ArrayList<Movie>();

    private SingletonOpening() {

    }

    public List<Movie> getInstance() {
        return movies;
    }

    @Override
    public String toString() {
        return "MoviesInfo : [movies=" + movies + "]";
    }
}
